import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Http,Response} from '@angular/http'
import { AssetsModelTransporterService } from "app/launchpad/assets/assets-model-transporter.service";
import { RoutingServices } from "app/common-services/routingServices.service";
import { HttpServices } from "app/common-services/httpServices.service";

@Component({
  selector: 'app-assets-3',
  templateUrl: './assets-3.component.html',
  styleUrls: ['./assets-3.component.scss']
})
export class Assets3Component implements OnInit {
  bankName: string;
  custName: any;
  summaryList: any;
  custId: string;
  total = 0;

  constructor(private assetTransporter: AssetsModelTransporterService, private routing: RoutingServices, private httpService: HttpServices) {
    this.summaryList = this.assetTransporter.accountList;
    this.custId = this.assetTransporter.custId;
    this.bankName = this.assetTransporter.bankDetails.bankName;
    this.custName = this.assetTransporter.custData.UserName;
    this.summaryList.forEach(element => {
      this.total += Number(element.availableBalance);
    });
  }
  ngOnInit() {
    
   }
  viewDetails(custId) {
    let url = 'https://sandbox.docitt.net/api/customer/' + custId + '/account/Filters';
    this.httpService.getData(url).subscribe(
      (response) => {
        this.assetTransporter.viewDataFilter = {};
        this.assetTransporter.viewDataFilter = response.json();
        this.routing.navigatePageUrl('launchpad/assets4');
      },
      (error) => {

      })
  }
}

