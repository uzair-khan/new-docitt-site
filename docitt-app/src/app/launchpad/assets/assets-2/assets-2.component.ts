import { ActivatedRoute } from '@angular/router';
import { forwardRef, Component, OnInit } from '@angular/core';
import { Response,ResponseContentType } from '@angular/http';
import { RoutingServices } from "app/common-services/routingServices.service";
import { Assets2Service } from "app/launchpad/assets/assets-2/assets-2.service";
import { AssetsModelTransporterService } from "app/launchpad/assets/assets-model-transporter.service";
import {Http} from '@angular/http' 
import * as FileSaver from 'file-saver'
@Component({
  selector: 'app-assets-2',
  templateUrl: './assets-2.component.html',
  styleUrls: ['./assets-2.component.scss'],
  providers: [Assets2Service]
})
export class Assets2Component implements OnInit {
  getData: any;
  imageSource: string;
  instituitionId: any;
  private resBody: any;
  private challengeResponse: any;
  user = '';
  pass = '';
  invitationId = '';
  providerId = '';
  private baseURL: string = 'https://sandbox.docitt.net/api/customer/';
  documentsList={};
  dowloadDocuments=false;
  docType=false;
  noDocuments=false;
  reqBody = {
    "CustomerId": "",
    "InvitationId": "",
    "InstitutionId": "",
    "Secert": this.pass,
    "UserName": this.user,
     "providers":''
  };
  responseBody = {
    data: "",
    flag: false,
    challengeId: ""
  };
  bankImage: { id: string, logo: string, name: string }
 
  options=[
    {
      "value":"592815bcc3310c123cc88aa4",
      "name":"Transaction data",
      "id":1
    },
    {
      "value":"59281677c3310c123cc88aa5",
      "name":"Document",
      "id":2
    }
  ];
 
  constructor(private assetsTransporter: AssetsModelTransporterService, private assetsService: Assets2Service, private routing: RoutingServices, private route: ActivatedRoute,private http:Http) {
    let bankImageModel: any;
    bankImageModel = this.assetsTransporter.bankDetails;
    this.instituitionId = this.assetsTransporter.bankDetails.bankId;

  }
  goBack() {
    this.routing.navigatePageUrl('launchpad/assets');
  }
  ngOnInit() {
    this.imageSource = this.assetsTransporter.bankDetails.imgUrl;
    
  }

  authenticate(f) {
    console.log("Inside the authenticate");
    this.user = f.value.onlineId;
    this.pass = f.value.password;
    this.reqBody.InvitationId = f.value.transaction.value;
    console.log(">>>>>>sss"+f.value.transaction.value);
    this.reqBody.Secert = this.pass;
    this.reqBody.UserName = this.user;
    let url2: string = "https://sandbox.docitt.net/api/company/59281487c3310c0d9ca0ac5e/invitation/"+this.reqBody.InvitationId;
    this.assetsService.getCall(url2).subscribe((response: Response) => {
      let url: string = this.baseURL + response.json().customerId + '/institution/' + this.instituitionId + '';
      this.reqBody.CustomerId = response.json().customerId;
      this.reqBody.InstitutionId = this.instituitionId;
      this.assetsTransporter.custData = this.reqBody;
      this.resBody = this.assetsService.authenticateUser(url, this.reqBody);
      console.log(">>> "+this.resBody);
       this.getCustomerDocuments(this.reqBody.CustomerId);
    },
      (error) => {
      })
  }

  getChallenges() {
    this.getInstitution();
    let url: string = this.baseURL +this.reqBody.CustomerId + '/account/1/challenge';
    this.challengeResponse = this.assetsService.getCall(url).subscribe(
      (response: Response) => {
        this.responseBody.data = "Enter the code:";
        this.responseBody.flag = true;
        this.responseBody.challengeId = response.json()[0].challengeId;
        console.log(">> challengeId >>> "+this.responseBody.challengeId);
        return this.responseBody;
      },
      (error)=>{
        console.log("Error in getting the challengeId ");
        console.log(error);
      }
    );
  }

  getInstitution() {
    let url: string = this.baseURL + this.reqBody.CustomerId + '/institution';
    this.assetsService.getCall(url).subscribe(
      (response: Response) => {
        let data = response.json()[0].providerId;
        this.providerId = data;
      }
    );
  }

  submitChallenge(form) {
    let body;

    if(this.reqBody.providers==='plaid'){
       body = {
      "ChallengeId": this.responseBody.challengeId,
      "CustomerId": this.reqBody.CustomerId,
      "InstitutionId": this.reqBody.InstitutionId,
      "InvitationId": this.reqBody.InvitationId,
      "ChallengeType": "Question",

       "Response":{
         "mfa":[form.value.otp]
        },
      "ProviderId": this.providerId
    }
  }
    else{
        body = {
      "ChallengeId": this.responseBody.challengeId,
      "CustomerId": this.reqBody.CustomerId,
      "InstitutionId": this.reqBody.InstitutionId,
      "InvitationId": this.reqBody.InvitationId,
      "ChallengeType": "Question",

      "Response": [ 
        { "id": "V2hhdCBpcyB5b3VyIG1vdGhlcidzIG1haWRlbiBuYW1lPw==", 
        "value": form.value.otp }
        ], 
      "ProviderId": this.providerId
    }
    }
    
    let url = this.baseURL + this.reqBody.CustomerId + '/institution/challenge';
    let result = this.assetsService.postOtp(url, body);


  }
  getCustomerDocuments(customerId){
    console.log("Getting documntes ID : >>>>");
   
    //URL hit to get the Documents List
    //https://sandbox.docitt.net/api/customer/{{customerid}}/document 
    //let url = this.baseURL + this.reqBody.CustomerId + 'document';
    let url = this.baseURL + customerId + '/document';
    this.assetsService.getCall(url).subscribe(
      (response: Response) => {
        
       this.documentsList= response.json();
       if(this.documentsList!==undefined)
        this.dowloadDocuments=true;
        else
        this.noDocuments=true;
      
      }
    );
  }
 onDownload(documentId,documentNo){
     let url = this.baseURL+this.reqBody.CustomerId+'/document/'+documentId+'/download?roleId=1';
     this.http.get(url,{ responseType: ResponseContentType.Blob }).subscribe(
       (response:Response)=>{
        
        FileSaver.saveAs(response.blob(),this.reqBody.CustomerId+"-"+documentNo+".pdf");
         
     },
       (error)=>{console.log(">>>Error "+error)}
     );
   }
  getSummary(cust) {
    let url: string = this.baseURL + cust + '/account/Summary';
    this.assetsTransporter.custData = this.reqBody;
    this.challengeResponse = this.assetsService.getCall(url).subscribe(
      (response: any) => {
        this.assetsTransporter.accountList = [];
        this.assetsTransporter.accountList = response.json();
        this.assetsTransporter.custId = cust;
        this.routing.navigatePageUrl('launchpad/assets3');
      }
    )
  }
  type = "password";
  changeVisual() {
    if (this.type === "password")
      this.type = "text";
    else
      this.type = "password";
  }
  OnSubmit(f) {
    console.log("Summit button isclicked");
    console.log();

  }
  onSelectChange(value){
    if(value.id==2){
      this.docType=true;
      this.reqBody.providers="fileThis";
    }else{
        this.docType=false;
         this.reqBody.providers="plaid";
    }
  }
}
