import { Component, OnInit } from '@angular/core';
import { RoutingServices } from "app/common-services/routingServices.service";
@Component({
  selector: 'app-profile-3',
  templateUrl: './profile-3.component.html',
  styleUrls: ['./profile-3.component.scss']
})
export class Profile3Component implements OnInit {

  constructor(private routingService:RoutingServices) { }

  ngOnInit() {
  }
goBack(){
  this.routingService.navigatePageUrl('');
}
loadNext(){
  this.routingService.navigatePageUrl('launchpad/assets');
}
}
